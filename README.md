# ndsview

![](https://img.shields.io/badge/written%20in-C%2B%2B%2C%20Javascript-blue)

A proof-of-concept desktop streaming hack.

Serves your Windows desktop to an HTML client. Source code included in download.


## Download

- [⬇️ ndsview-0.1b.7z](dist-archive/ndsview-0.1b.7z) *(164.65 KiB)*
- [⬇️ ndsview-0.1a.7z](dist-archive/ndsview-0.1a.7z) *(87.72 KiB)*
